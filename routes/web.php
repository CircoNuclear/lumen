<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {

    // Criar novo usuário
    $router->post('/users', 'UserController@createUser');
    
    // Verificacao de contar
    $router->get('/verification-account/{token}', 'UserController@verification');

    // Resetar a senha ( pra terminar )
    $router->post('/resetar', 'UserController@resetarSenha');

    $router->group(['middleware' => ['cors']], function () use ($router) {

        // Login Route
        $router->post('/login',  'UserController@login');

        
    });
    
    
    $router->group(['middleware' => ['auth', 'cors', 'is-verified']], function () use ($router) {
        $router->post('/get/user',  'UserController@getUser'); 
    });



    $router->group(['middleware' => ['auth', 'token-expired', 'is-verified', 'cors', 'admin']], function () use ($router) {

        // Get Clientes
        $router->post('/clientes', 'ClienteController@getClientes');

        $router->get('/user-auth', function (Request $request) {  
            return $request->user();
        });
        
    });

});
