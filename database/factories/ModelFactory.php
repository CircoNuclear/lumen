<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Model\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => \Hash::make('secret'),
        'verified' => true
    ];
});


$factory->define(App\Model\Login\Menu::class, function (Faker\Generator $faker) {
    return [
        'id' => $faker->randomDigit,
        'menu' => $faker->name 
    ];
});
