<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuAcessoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_acesso', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_menu');
            $table->foreign('id_menu')->references('id')->on('menu');
            $table->bigInteger('id_user_tipo');
            $table->foreign('id_user_tipo')->references('id')->on('user_tipo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_acesso');
    }
}
