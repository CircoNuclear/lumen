<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('ClienteTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('TipoTableSeeder');
        $this->call('MenuTableSeeder');
        $this->call('UserTipoSeeder');
        $this->call('MenuAcessoTableSeeder');
    }
}