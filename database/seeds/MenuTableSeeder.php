<?php

use Illuminate\Database\Seeder;
use \App\Model\Login\Menu;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        factory(\App\Model\Login\Menu::class, 1)->create(
            [
                'id' => 1,
                'menu'  => 'monitoramento'
            ] 
        );

        factory(\App\Model\Login\Menu::class, 1)->create(
            [
                'id' => 2,
                'menu'  => 'filas'
            ]
        );

        factory(\App\Model\Login\Menu::class, 1)->create(
            [
                'id' => 3,
                'menu'  => 'seguranca'
            ]
        );

        factory(\App\Model\Login\Menu::class, 1)->create(
            [
                'id' => 4,
                'menu'  => 'administracao'
            ]
        ); 

    }
}
