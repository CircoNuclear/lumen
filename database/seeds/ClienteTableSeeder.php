<?php

use Illuminate\Database\Seeder;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cliente')->insert([
            'id'    => 1,
            'nome' => 'Voip Easy',
            'email' => 'voipeasy@voipeasy.com'
        ]);
    }
}
