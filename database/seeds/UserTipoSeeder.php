<?php

use Illuminate\Database\Seeder;

class UserTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_tipo')->insert([
            'id' => 1,
            'id_user' => 1,
            'id_tipo' => 1
        ]);
    }
}
