<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\Model\User::class, 1)->create([
            'id' => 1,
            'id_cliente' => 1,
            'is_admin' => true,
            'email' => 'luiz.lgvasconceslos2@gmail.com',
            'password'  => \Hash::make('123456789'),
            'verified' => true
        ]);
        

        // factory(\App\User::class, 20)->create();

    }
}