<?php

use Illuminate\Database\Seeder;

class MenuAcessoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_acesso')->insert([
            'id'    => 1,
            'id_user_tipo' => 1,
            'id_menu' => 1,
        ]);

        DB::table('menu_acesso')->insert([
            'id'    => 2,
            'id_user_tipo' => 1,
            'id_menu' => 2,
        ]);

        DB::table('menu_acesso')->insert([
            'id'    => 3,
            'id_user_tipo' => 1,
            'id_menu' => 3,
        ]);

        DB::table('menu_acesso')->insert([
            'id'    => 4,
            'id_user_tipo' => 1,
            'id_menu' => 4,
        ]);
    }
}
