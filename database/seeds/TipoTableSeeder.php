<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class TipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo')->insert(
            [
                'id' => 1,
                'tipo' => 'Administrador'
            ]
        );

        DB::table('tipo')->insert(
            [
                'id' => 2,
                'tipo' => 'Supervisor'
            ]
        );

        DB::table('tipo')->insert(
            [
                'id' => 3,
                'tipo' => 'Comum'
            ]
        ); 
 
    }
}
