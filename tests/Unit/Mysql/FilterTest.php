<?php

use App\Model\Teste\Teste;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class FilterTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testWhere()
    {

        $filters = new Teste;
        $filters->where('id', '=', 1);

        $actual = $filters->getSql();
        $expected = 'WHERE id=1';
        
        $this->assertEquals($expected, $actual);
        
        // $this->get('/');
        // $this->assertEquals(
        //     $this->app->version(), $this->response->getContent()
        // );

    }

    public function testeOrderBy()
    {

        $filters = new Teste;
        $filters->orderBy('created', 'desc');

        $actual = $filters->getSql();
        $expected = 'ORDER BY created desc';

        $this->assertEquals($expected, $actual);

    }

    public function testeOrderByAndSelect()
    {

        $filters = new Teste;
        $filters->where('id', '=', 1);
        $filters->orderBy('created', 'desc');

        $actual = $filters->getSql();
        $expected = 'WHERE id=1 ORDER BY created desc';
        
        $this->assertEquals($expected, $actual);

    }
}
