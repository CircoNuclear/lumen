<?php

namespace App\Http\Controllers;

use App\Model\Cliente;

use Illuminate\Http\Request;

class ClienteController extends Controller
{

    public function __construct(Cliente $cliente){
        $this->cliente = $cliente;
    }

    public function getClientes(Request $request){

        return $this->cliente->getClientes();
    }



}