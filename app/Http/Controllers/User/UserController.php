<?php

namespace App\Http\Controllers;

use App\Model\User;

use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct(User $user){
        $this->user = $user;
    }

    public function createUser( Request $request ){

        $this->validate($request, [
            'name'       => 'required|max:255',
            'id_cliente' => 'required|exists:cliente,id',
            'email'      => 'required|email|max:255|unique:user',
            'password'   => 'required|min:6|max:16|confirmed',
            'redirect'   => 'required|url'
        ]);

        $data = $request->all();
        $data['password'] = \Hash::make($data['password']);

        $model = $this->user::create($data);
        $model->verification_token = md5(mt_rand());
        $model->save();

        $redirect = route('verification_account', [
            'token' => $model->verification_token,
            'redirect' => $request->get('redirect') 
        ]);

        \Notification::send($model, new \App\Notifications\AccountCreated($model, $redirect));

        return response()->json($model, 201);

    }

    public function resetarSenha(Request $request){ 

        $this->validate($request, [ 
            'email' => 'required|email|max:255|unique:user'
        ]);

        $this->user::where('email', $request->email)->firstOrFail(); 

        $expiration = new \Carbon\Carbon();
        $expiration->addHour(1);
        $user->reset_token = sha1( mt_rand() ).'.'.sha1( mt_rand() );
        $user->reset_token_expiration = $expiration->format('Y-m-d H:i:s');
        $user->save();
    
        $redirect = route('change_password', [
            'token' => $model->verification_token,
            'redirect' => $request->get('redirect') 
        ]
        );

        \Notification::send($user, new \App\Notifications\AccountCreated($user, $redirect));
    
        return response()->json($model, 201); 
    }

    public function verification(Request $request, $token){

        $user = $this->user::where('verification_token', $token)->firstOrFail();
        $user->verified = true;
        $user->verification_token = null;
        $user->save();
        $redirect =     $request->get('redirect');
        return redirect()->to($redirect);

    }

    public function login(Request $request){

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $email = $request->get('email');
        $password = $request->get('password');
        $user = $this->user::where('user.email', '=', $email)->join('cliente', 'cliente.id', '=', 'user.id_cliente')->select('user.id', 'user.password', 'user.api_token', 'user.api_token_expiration')->first();
 
        if (!$user || !\Hash::check($password, $user->password)) {
            return response()->json(['message' => 'Invalid credentials'], 400);
        }

        $menus =  $this->user::where('user.id', $user->id )->join('user_tipo', 'user_tipo.id_user', '=', 'user.id')->join('menu_acesso', 'menu_acesso.id_user_tipo', '=', 'user_tipo.id')->join('menu', 'menu.id', '=', 'menu_acesso.id_menu')->get();
 
        $permissao = [];

        if(!empty($menus))
        {
            foreach($menus as $menu)
            { 
                $permissao[$menu->menu] = true; 
            }
        } 

        $expiration = new \Carbon\Carbon();
        $expiration->addHour(1);
        $user->api_token = sha1( mt_rand() ).'.'.sha1( mt_rand() );
        $user->api_token_expiration = $expiration->format('Y-m-d H:i:s');
        $user->save();
        
        return response()->json(
            [
             'api_token' => $user->api_token, 
             'api_token_expiration' => $user->api_token_expiration,
             'menu' => $permissao
            ], 200);
    }

    public function getUser(Request $request){

        $api_token = $request['api_token'];
        return $this->user->getUser($api_token);
    }

}