<?php

namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Notifications\Notifiable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, Notifiable;

    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id_cliente', 'name', 'email', 'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */public function getClientes(){
        
        return Cliente::select($this->selectClientes())
            ->get();    
    }
    protected $hidden = [
        'password','api_token', 'api_token_expiration'
    ];

    public function selectUser(){
        return [
            'id_cliente', 'name', 'email'
        ];
    }

    public function getUser($api_token){

        $user = User::where('api_token', $api_token)
            ->select( 'user.id_cliente', 'user.name', 'user.email' )
            ->first();
        
        info($user);

        return $user;
    }
}
