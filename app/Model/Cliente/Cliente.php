<?php

namespace App\Model;
 
use Illuminate\Database\Eloquent\Model; 

class Cliente extends Model
{ 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','api_token', 'api_token_expiration'
    ];

    protected $table = 'cliente';

    public function selectClientes(){
        return [
            'id', 'nome', 'email', 'telefone', 'celular', 'ativo'
        ];
    }

    public function getClientes(){
        
        return Cliente::select($this->selectClientes())
            ->get();    
    }

    // public function getCliente(Request $request){
        
    //     return Cliente::select($this->selectClientes())
    //         ->get();    
    // }

}
