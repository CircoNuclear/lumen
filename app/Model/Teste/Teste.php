<?php

namespace App\Model\Teste;
   
use Illuminate\Database\Eloquent\Model; 

class Teste extends Model{
     
    protected $table = 'teste';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id', 'teste', 'email', 'password'
    ];

    private $sql = []; 

    public function where( string $field, string $condition, $valor){

        $where = 'WHERE %s%s%s';
        $this->sql[] = sprintf($where, $field, $condition, $valor); 
        return $this;

    }

    public function getSql(){
        return implode( ' ', $this->sql); 
    }

    public function orderBy($field, $order){
        $orderBy = "ORDER BY %s %s";
        $this->sql[] = sprintf($orderBy, $field, $order);
        return $this;
    }
}
